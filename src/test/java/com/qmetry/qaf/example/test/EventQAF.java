package com.qmetry.qaf.example.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.qmetry.qaf.automation.step.WsStep;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.ws.rest.RestTestBase;
import com.qmetry.qaf.utility.BaseUtility;

public class EventQAF extends BaseUtility {

	String Event_id;
	Map<String, Object> reqdata = new HashMap<String, Object>();

	@Test(priority = 1)
	public void getListEvent() {
		WsStep.userRequests("event.list.get.request");
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		String listeventid = "$.data[*].id";
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
		List<String> listEventID = namejsonContext.read(listeventid);
		System.out.println("The LIST of EVNETS ID  : " + listEventID);
		WsStep.responseShouldHaveStatus("OK");
		WsStep.responseShouldHaveStatusCode(200);
	}
   @Test(priority = 2)
   public void createEvent() {
	   WsStep.userRequests("event.create.post.request",reqdata);
	   String resposebody = new RestTestBase().getResponse().getMessageBody();
		String eventid = "$.data.id";
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
	    Event_id=namejsonContext.read(eventid);
		BaseUtility.changePropertyValue("Eventid", Event_id);
		System.out.println("the Role id "+Event_id);
		WsStep.responseShouldHaveStatusCode(201);
   }
   @Test(priority = 3)
   public void getEvent() {
	   reqdata.put("eventendpoint",BaseUtility.readPropertyFile("Eventid"));
	   WsStep.userRequests("event.get.request",reqdata);
	   String resposebody = new RestTestBase().getResponse().getMessageBody();
		String eventid = "$.data.id";
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
		Event_id=namejsonContext.read(eventid);
		WsStep.responseShouldHaveStatusCode(200);
		Validator.assertTrue(Event_id.equalsIgnoreCase(BaseUtility.readPropertyFile("Eventid")), "EVENT id is incorrect", "Correct EVENT ID");
   }
   @Test(priority = 4)
   public void deleterole() {
		reqdata.put("eventendpoint", BaseUtility.readPropertyFile("Eventid"));
		WsStep.userRequests("event.delete.request",reqdata);
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveValueIgnoringCase("Object successfully deleted", "meta.message");
		BaseUtility.changePropertyValue("Eventid", "");
	}
}
