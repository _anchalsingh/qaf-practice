package com.qmetry.qaf.example.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.qmetry.qaf.automation.step.WsStep;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.ws.rest.RestTestBase;
import com.qmetry.qaf.utility.BaseUtility;

public class RoleInviteQAF extends BaseUtility {
	String Role_invite_id;
	Map<String, Object> reqdata = new HashMap<String, Object>();

	@Test(priority = 1)
	public void getListRoleInvite() {
		reqdata.put("eventendpoint", BaseUtility.readPropertyFile("Eventid"));
		WsStep.userRequests("roleinvite.list.request", reqdata);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		String listroleinviteid = "$.data[*].id";
		DocumentContext jsonreader = JsonPath.parse(resposebody);
		List<String> listRoleinviteId = jsonreader.read(listroleinviteid);
		System.out.println("The list of role invite : " + listRoleinviteId);
		WsStep.responseShouldHaveStatusCode(200);
	}

	@Test(priority = 2)
	public void createRoleInvite() {
		WsStep.userRequests("roleinvite.create.request");
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		String roleinvite = "$.data.id";
		DocumentContext jsonreader = JsonPath.parse(resposebody);
		Role_invite_id = jsonreader.read(roleinvite);
		System.out.println("The ROLE Invite id " + Role_invite_id);
		WsStep.responseShouldHaveStatusCode(201);
	}
	@Test(priority = 3)
	public void getRoleInvite() {
		reqdata.put("roleinviteid",BaseUtility.readPropertyFile("RoleInviteId"));
		WsStep.userRequests("roleinvite.get.request", reqdata);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		String roleinvite = "$.data.id";
		DocumentContext jsonreader = JsonPath.parse(resposebody);
		Role_invite_id = jsonreader.read(roleinvite);
		WsStep.responseShouldHaveStatusCode(200);
		Validator.assertTrue(Role_invite_id.equalsIgnoreCase(BaseUtility.readPropertyFile("RoleInviteId")), "incorrect Roleinvite id" , "Correct Roleinvite id ");
	}
}
