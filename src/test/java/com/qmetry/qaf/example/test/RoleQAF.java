package com.qmetry.qaf.example.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.qmetry.qaf.automation.step.WsStep;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.ws.rest.RestTestBase;
import com.qmetry.qaf.utility.BaseUtility;

public class RoleQAF extends BaseUtility {

	public String Role_id;
	Map<String, Object> reqdata = new HashMap<String, Object>();
	

	@Test(priority = 1)
	public void listRoles() {
		WsStep.userRequests("role.list.get.request");	
				String resposebody = new RestTestBase().getResponse().getMessageBody();
		String listroleid = "$.data[*].id";
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
		List<String> listRoleId = namejsonContext.read(listroleid);
		System.out.println("The LIST of ROLE ID  : " + listRoleId);
		WsStep.responseShouldHaveStatus("OK");
		WsStep.responseShouldHaveStatusCode(200);
	}
	@Test(priority = 2)
	public void createRole() {
		 String rname = BaseUtility.randomString();
		reqdata.put("rolename", rname);
		WsStep.userRequests("role.create.post.request", reqdata);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		String role_id = "$.data.id";
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
	    Role_id=namejsonContext.read(role_id);
		BaseUtility.changePropertyValue("roleid", Role_id);
		BaseUtility.changePropertyValue("rolenamedata", rname);
		System.out.println("the role id :"+Role_id);
		System.out.println("the role name :"+rname);
		WsStep.responseShouldHaveStatusCode(201);
	}
	@Test(priority = 3)
	public void getRole() {
		reqdata.put("roleidendpoint", BaseUtility.readPropertyFile("roleid"));
		reqdata.put("token", properties.getProperty("accesstoken"));
		WsStep.userRequests("role.get.request",reqdata);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		String roleid = "$.data.id";
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
		Role_id=namejsonContext.read(roleid);
		WsStep.responseShouldHaveStatusCode(200);
		Validator.assertTrue(Role_id.equalsIgnoreCase(BaseUtility.readPropertyFile("roleid")), "ROLE id is incorrect", "Correct ROLE ID");
	}
	@Test(priority = 4)
	public void deleterole() {
		reqdata.put("roleidendpoint", BaseUtility.readPropertyFile("roleid"));
		reqdata.put("token", properties.getProperty("accesstoken"));
		WsStep.userRequests("role.delete.request",reqdata);
		WsStep.responseShouldHaveStatusCode(200);
		BaseUtility.changePropertyValue("roleid", "");
	}
}
