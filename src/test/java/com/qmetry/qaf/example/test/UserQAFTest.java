package com.qmetry.qaf.example.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.testng.annotations.Test;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.qmetry.qaf.automation.step.WsStep;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.ws.rest.RestTestBase;
import com.qmetry.qaf.utility.BaseUtility;

public class UserQAFTest extends BaseUtility {
	public String User_id;
	Map<String, Object> reqdata = new HashMap<String, Object>();

	@Test(priority = 1)
	public void listUser() {
		WsStep.userRequests("user.list.get.request");
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		String listuserid = "$.data[*].id";
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
		List<String> listUserId = namejsonContext.read(listuserid);
		System.out.println("The LIST of USER ID  : " + listUserId);
		WsStep.responseShouldHaveStatus("OK");
		WsStep.responseShouldHaveStatusCode(200);
	}

	@Test(priority = 2)
	public void createUser() {
		WsStep.userRequests("user.create.post.request");
		WsStep.responseShouldHaveStatusCode(201);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		String userid = "$.data.id";
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
		User_id = namejsonContext.read(userid);
		System.out.println("the user id :" + User_id);
		BaseUtility.changePropertyValue("Userid", User_id);
		WsStep.responseShouldHaveStatusCode(201);
	}

	@Test(priority = 3)
	public void getUserDetail() {
		reqdata.put("useridendpoint", BaseUtility.readPropertyFile("Userid"));
		WsStep.userRequests("user.get.request", reqdata);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		String userid = "$.data.id";
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
		User_id = namejsonContext.read(userid);
		WsStep.responseShouldHaveStatusCode(200);
		Validator.assertTrue(User_id.equalsIgnoreCase(BaseUtility.readPropertyFile("Userid")), "USER ID is incorrect",
				"Correct USER ID");

	}

	@Test(priority = 4)
	public void deleteUserDetail() {
		reqdata.put("useridendpoint", BaseUtility.readPropertyFile("Userid"));
		WsStep.userRequests("user.delete.request", reqdata);
		WsStep.responseShouldHaveStatusCode(200);
		BaseUtility.changePropertyValue("Userid", "");
	}
}
