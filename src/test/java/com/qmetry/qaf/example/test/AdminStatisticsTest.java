package com.qmetry.qaf.example.test;

import org.testng.annotations.Test;
import com.qmetry.qaf.automation.step.WsStep;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.ws.RestWSTestCase;
import com.qmetry.qaf.automation.ws.rest.RestTestBase;
import com.qmetry.qaf.utility.BaseUtility;
public class AdminStatisticsTest extends RestWSTestCase{
	
	@Test
	public void getEventStatisticDetails() {
		WsStep.userRequests("event.statistics.events");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveHeaderWithValue(BaseUtility.contentTypeHeader, BaseUtility.apiJsonValue);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		Reporter.log("Response Body : "+resposebody);
		Validator.assertTrue(resposebody.contains("data"), "Event statistics not found.", "Event statistics generated successfully");
	}
	
	@Test
	public void getEventTypes() {
		WsStep.userRequests("event.statistics.types");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveHeaderWithValue(BaseUtility.contentTypeHeader, BaseUtility.contentHeaderValue);	
	}
	
	@Test
	public void getStatisticsUsers() {
		WsStep.userRequests("event.statistics.users");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveHeaderWithValue(BaseUtility.contentTypeHeader, BaseUtility.apiJsonValue);	
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		Reporter.log("Response Body : "+resposebody);
		Validator.assertTrue(resposebody.contains("data"), "Users statistics not found.", "Users statistics generated successfully");
	}
	
	@Test
	public void getSessionStatistics() {
		WsStep.userRequests("event.statistics.sessions");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveHeaderWithValue(BaseUtility.contentTypeHeader, BaseUtility.apiJsonValue);	
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		Reporter.log("Response Body : "+resposebody);
		Validator.assertTrue(resposebody.contains("data"), "Users statistics not found.", "Users statistics generated successfully");
	}
	
	@Test
	public void getMailsStatistics() {
		WsStep.userRequests("event.statistics.mails");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveHeaderWithValue(BaseUtility.contentTypeHeader, BaseUtility.apiJsonValue);	
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		Reporter.log("Response Body : "+resposebody);
		Validator.assertTrue(resposebody.contains("data"), "Users statistics not found.", "Users statistics generated successfully");
	}

}
