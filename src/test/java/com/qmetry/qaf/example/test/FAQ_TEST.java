package com.qmetry.qaf.example.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.qmetry.qaf.automation.step.WsStep;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.ws.rest.RestTestBase;
import com.qmetry.qaf.utility.BaseUtility;

public class FAQ_TEST extends BaseUtility {
	Map<String, Object> reqdata = new HashMap<String, Object>();
     String faq_id;
	@Test(priority = 1)
	public void createFAQ() {
		reqdata.put("Eventid", BaseUtility.readPropertyFile("Eventid"));
		WsStep.userRequests("FAQ.create.request",reqdata);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		DocumentContext jsonreader = JsonPath.parse(resposebody);
		faq_id=jsonreader.read("$.data.id");
		System.out.println("the FAQ ID :"+faq_id);
		BaseUtility.changePropertyValue("FAQid", faq_id);
		WsStep.responseShouldHaveStatusCode(201);
	}
	@ Test(priority = 2)
	public void getFAQDetails() {
		reqdata.put("FAQendpoint", BaseUtility.readPropertyFile("FAQid"));
		WsStep.userRequests("FAQ.get.request",reqdata);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		DocumentContext jsonreader = JsonPath.parse(resposebody);
		faq_id = jsonreader.read("$.data.id");
		System.out.println("the  FAQ ID :"+faq_id);
		Validator.assertTrue(faq_id.equalsIgnoreCase(BaseUtility.readPropertyFile("FAQid")), "Incorrect FAQ ID ", "Correct FAQ ID");
		
	}
	@Test(priority = 3)
	public void listFAQOfEvent() {
		reqdata.put("eventendpoint", BaseUtility.readPropertyFile("Eventid"));
		WsStep.userRequests("FAQ.list.event.request",reqdata);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
		List<String> listFAQID = namejsonContext.read("$.data[*].id");
		System.out.println("The LIST of FAQ ID : " + listFAQID);
		WsStep.responseShouldHaveStatus("OK");
		WsStep.responseShouldHaveStatusCode(200);
	}
	@Test(priority = 4)
	public void deleteFAQ() {
		reqdata.put("FAQendpoint", BaseUtility.readPropertyFile("FAQid"));
		WsStep.userRequests("FAQ.delete.request",reqdata);
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveValueIgnoringCase("Object successfully deleted", "meta.message");
		BaseUtility.changePropertyValue("FAQid", "");
	}
	
}

