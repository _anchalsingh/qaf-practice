package com.qmetry.qaf.example.test;

import java.io.IOException;
import org.testng.annotations.Test;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.qmetry.qaf.automation.step.WsStep;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.ws.RestWSTestCase;
import com.qmetry.qaf.automation.ws.rest.RestTestBase;
import com.qmetry.qaf.utility.BaseUtility;

public class AuthenticationTest extends RestWSTestCase {
	public static String token = "$.access_token";
	
	@Test(description = "To verify login authentication.", priority = 0)
	public void loginAuth() throws IOException {
		WsStep.userRequests("auth.post.request");
		WsStep.responseShouldHaveStatus("OK");
		WsStep.responseShouldHaveStatusCode(200);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
		String accesstoken = namejsonContext.read(token);
		BaseUtility.changePropertyValue("accesstoken", accesstoken);
	}
	
	@Test(description = "To verify login authentication with remember me", priority = 1)
	public void loginAuthWithRememberMe() {
		WsStep.userRequests("auth.post.requestWithRemember");
		WsStep.responseShouldHaveStatus("OK");
		WsStep.responseShouldHaveHeaderWithValue(BaseUtility.contentTypeHeader, BaseUtility.contentHeaderValue);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		Validator.assertTrue(resposebody.contains("access_token"), "Access token not found.", "Access token generated successfully");
	}
	
	@Test(description = "To verify login authentication for mobile client", priority = 2)
	public void loginAuthMobile() {
		WsStep.userRequests("auth.post.requestMobile");
		WsStep.responseShouldHaveStatus("OK");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveHeaderWithValue(BaseUtility.contentTypeHeader, BaseUtility.contentHeaderValue);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		Validator.assertTrue(resposebody.contains("access_token"), "Access token not found.", "Access token generated successfully");
	}
	
	@Test(description = "To verify Re-authentication.", priority = 3)
	public void reAuthentication() {
		String accessToken = BaseUtility.getAccessToken();
		String tokenJWT = "JWT "+accessToken;
		BaseUtility.changePropertyValue("token", tokenJWT);
		WsStep.userRequests("auth.post.requestReAuthentication");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveHeaderWithValue(BaseUtility.contentTypeHeader, BaseUtility.contentHeaderValue);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		Validator.assertTrue(resposebody.contains("access_token"), "Access token not found.", "Access token generated successfully");
	}
	
	@Test(description = "To verify logout.", priority = 4)
	public void verifyLogout() {
		WsStep.userRequests("auth.post.requestLogout");
		WsStep.responseShouldHaveStatusCode(200);
		WsStep.responseShouldHaveHeaderWithValue(BaseUtility.contentTypeHeader, BaseUtility.contentHeaderValue);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		Validator.assertTrue(resposebody.contains("success"), "User is not able to logout.", "User logout successfully.");
	}
}
