package com.qmetry.qaf.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang.math.RandomUtils;
import org.testng.annotations.BeforeTest;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.qmetry.qaf.automation.step.WsStep;
import com.qmetry.qaf.automation.ws.rest.RestTestBase;

public class BaseUtility {

	
	
	public static Properties properties = new Properties();
	public static FileInputStream fileinput = null;
	public static String contentTypeHeader = "Content-Type";
	public static String contentHeaderValue ="application/json";
	public static String apiJsonValue = "application/vnd.api+json";
	

	public static void changePropertyValue(String key, String value) {

		FileOutputStream fileoutput;
		try {
			fileinput = new FileInputStream(System.getProperty("user.dir") + "\\resources\\TestData\\Data.properties");
			properties.load(fileinput);
			fileinput.close();
		} catch (FileNotFoundException e) {
			System.out.println("Properties file is not found");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			fileoutput = new FileOutputStream(
					System.getProperty("user.dir") + "\\resources\\TestData\\Data.properties");
			properties.setProperty(key, value);
			properties.store(fileoutput, null);
			fileoutput.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Properties file is not udpated");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String readPropertyFile(String key) {
		String keyvalue = null;
		try {
			fileinput = new FileInputStream(System.getProperty("user.dir") + "\\resources\\TestData\\Data.properties");
			properties.load(fileinput);
			keyvalue = properties.getProperty(key);
		} catch (FileNotFoundException e) {
			System.out.println("Application properties file not found");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return keyvalue;
	}

	public static String randomString() {
		return "Anchal" + RandomUtils.nextInt(50);
	}

	@BeforeTest
	public static void generteToken() {
		WsStep.userRequests("auth.post.request");
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		String tokenValue = "JWT " + JsonPath.read(resposebody, "$.access_token");
		changePropertyValue("token", tokenValue);
	}
	
	public static String getAccessToken() {
		WsStep.userRequests("auth.post.request");
		WsStep.responseShouldHaveHeaderWithValue(BaseUtility.contentTypeHeader, BaseUtility.contentHeaderValue);
		String resposebody = new RestTestBase().getResponse().getMessageBody();
		DocumentContext namejsonContext = JsonPath.parse(resposebody);
		String token = "$.access_token";
		String accesstoken = namejsonContext.read(token);
		return accesstoken;
	}

}
